/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import static Models.Base.query;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class Loaning implements Base{
    private final String tableName="loaning";
    private Integer id;
    private Integer userId;
    private Integer roomId;
    private String startTime;
    private String endTime;
    private Integer deleted;

    /**
    * No args constructor for use in serialization
    * 
    */
    public Loaning() {
    }

    /**
    * 
    * @param startTime
    * @param id
    * @param userId
    * @param roomId
    * @param endTime
    * @param deleted
    */
    public Loaning(Integer id, Integer userId, Integer roomId, String startTime, String endTime, Integer deleted) {
    super();
    this.id = id;
    this.userId = userId;
    this.roomId = roomId;
    this.startTime = startTime;
    this.endTime = endTime;
    this.deleted = deleted;
    }

    public Integer getId() {
    return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }

    public Integer getUserId() {
    return userId;
    }

    public void setUserId(Integer userId) {
    this.userId = userId;
    }

    public Integer getRoomId() {
    return roomId;
    }

    public void setRoomId(Integer roomId) {
    this.roomId = roomId;
    }

    public String getStartTime() {
    return startTime;
    }

    public void setStartTime(String startTime) {
    this.startTime = startTime;
    }

    public String getEndTime() {
    return endTime;
    }

    public void setEndTime(String endTime) {
    this.endTime = endTime;
    }

    public Integer getDeleted() {
    return deleted;
    }

    public void setDeleted(Integer deleted) {
    this.deleted = deleted;
    }

    public Object[] getDataField(){
        Object[] obj = {id, userId, roomId, startTime, endTime, deleted};
        return obj;
    }

    @Override
    public ResultSet getAll() {
        ResultSet res = null;
        try {
            res = query.getAll(tableName, "where deleted=0 order by id asc");
        } catch (SQLException ex) {
            Logger.getLogger(Facility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    @Override
    public ResultSet get() {
        ResultSet res = null;
        try {
            res = query.getAll(tableName, "where deleted=0 AND id='"+id+"' order by id asc");
        } catch (SQLException ex) {
            Logger.getLogger(Facility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    @Override
    public void insert() {
        Object[] obj = {id, userId, roomId, startTime, endTime, deleted};
        try {
            query.post(tableName, obj);
            createCron();
        } catch (SQLException ex) {
            Logger.getLogger(Facility.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            query.del(tableName, id);
        } catch (SQLException ex) {
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Integer id) {
        Object[] column = { "userId", "roomId", "startTime", "endTime", "deleted"};
        Object[] data = {userId, roomId, startTime, endTime, deleted};
        try {
            query.put(tableName, id, column, data);
        } catch (SQLException ex) {
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void createCron(){
        try{
            PreparedStatement pst = query.ddlRaw("CREATE EVENT "+userId+"_"+roomId+" on SCHEDULE AT '"+endTime+"' DO update room set status = 'Available' where id='"+roomId+"'");
            pst.execute();
            pst.close();
        }
        catch(SQLException ex){
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
