/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import static Models.Base.query;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class Room implements Base{
    private final String tableName = "room";
    private final String viewName = "";
    private final LinkedList<Room> data = new LinkedList<>();
    private final LinkedList<Facility> roomFacilities = new LinkedList<>();
    private Room selectedData = this;
    private Integer id;
    private String code;
    private String name;
    private String description;
    private Integer coverage;
    private String status;
    private Integer buildingId;
    private Integer deleted;
    private Loaning latestLoaning = new Loaning();

    /**
    * No args constructor for use in serialization
    * 
    */
    public Room() {
    }

    /**
    * 
    * @param id
    * @param status
    * @param coverage
    * @param description
    * @param name
    * @param code
    * @param deleted
    * @param buildingId
    */
    public Room(Integer id, String code, String name, String description, Integer coverage, String status, Integer buildingId, Integer deleted) {
    super();
    this.id = id;
    this.code = code;
    this.name = name;
    this.description = description;
    this.coverage = coverage;
    this.status = status;
    this.buildingId = buildingId;
    this.deleted = deleted;
    }

    public Integer getId() {
    return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }

    public String getCode() {
    return code;
    }

    public void setCode(String code) {
    this.code = code;
    }

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

    public String getDescription() {
    return description;
    }

    public void setDescription(String description) {
    this.description = description;
    }

    public Integer getCoverage() {
    return coverage;
    }

    public void setCoverage(Integer coverage) {
    this.coverage = coverage;
    }

    public String getStatus() {
    return status;
    }

    public void setStatus(String status) {
    this.status = status;
    }

    public Integer getBuildingId() {
    return buildingId;
    }

    public void setBuildingId(Integer buildingId) {
    this.buildingId = buildingId;
    }

    public Integer getDeleted() {
    return deleted;
    }

    public void setDeleted(Integer deleted) {
    this.deleted = deleted;
    }
    
    public LinkedList getRoomFacilites(){
        return this.roomFacilities;
    }
    
    public void addRoomFacilityData(Facility data){
        this.roomFacilities.add(data);
    }
    
    public void clearRoomFacilities(){
        this.roomFacilities.clear();
    }
    
    public LinkedList getData(){
        return this.data;
    }
    
    public void addData(Room data){
        this.data.add(data);
    }

    public Room getSelectedData(){
        return this.selectedData;
    }
    
    public void setSelectedData(Room param){
        this.selectedData = param;
    }
    
    public void clearData(){
        this.data.clear();
    }
    
    public void setLatestLoaning(Loaning param){
        latestLoaning = param;
    }
    
    public Loaning getLatestLoaning(){
        return latestLoaning;
    }
    
    @Override
    public ResultSet getAll() {
        ResultSet res = null;
        try {
            res = query.getAll(tableName, "where deleted=0 order by id asc");
        } catch (SQLException ex) {
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    @Override
    public ResultSet get(){
        ResultSet res = null;
        try{
            res = query.getAll(tableName, "where deleted=0 AND id='"+id+"' order by id asc");
        }
        catch(SQLException ex){
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    @Override
    public void insert() {
        Object[] obj = {id, code, name, description, coverage, status, buildingId, deleted};
        try {
            query.post(tableName,obj);
        } catch (SQLException ex) {
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void delete(Integer id) {
        try {
            query.del(tableName, id);
        } catch (SQLException ex) {
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Integer id) {
        Object[] column = {"code","name", "description", "coverage", "status", "buildId"};
        Object[] data = {code, name, description, coverage, status, buildingId};
        try {
            query.put(tableName, id, column, data);
        } catch (SQLException ex) {
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public String toString(){
        return this.name;
    }
    
    public ResultSet getRoomFacilitiesQuery(){
        ResultSet res = null;
        try{
            res = query.raw("SELECT facility.*, roomfacility.qty from roomfacility \n" +
                            "join room on room.id = roomfacility.roomId\n" +
                            "join facility on facility.id = roomfacility.facilityId\n" +
                            "where roomfacility.deleted = 0 AND room.id = "+this.selectedData.getId());
        }
        catch(SQLException ex){
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    };
    
    public void getLatestLoaningQuery(){
        ResultSet res = null;
        initLatestLoaning();
        try{
            res = query.raw("SELECT * from loaning where deleted = 0 AND roomId = "+id+" and roomId IN (SELECT roomId FROM loaning WHERE endtime = (SELECT MAX(endtime) FROM loaning) or endtime) ORDER BY id DESC limit 1");
//            System.out.println(res.first());
                while(res.next()){
                    latestLoaning.setId(res.getInt("id"));
                    latestLoaning.setRoomId(res.getInt("roomId"));
                    latestLoaning.setUserId(res.getInt("userId"));
                    latestLoaning.setStartTime(res.getString("startTime"));
                    latestLoaning.setEndTime(res.getString("endTime"));
                    latestLoaning.setDeleted(res.getInt("deleted"));
                    System.out.println(res.getString("endTime"));
                }
//            }
//            else{
//                    latestLoaning.setId(res.getInt("id"));
//                    latestLoaning.setRoomId(res.getInt("roomId"));
//                    latestLoaning.setUserId(res.getInt("userId"));
//                    latestLoaning.setStartTime(res.getString("startTime"));
//                    latestLoaning.setEndTime(res.getString("endTime"));
//                    latestLoaning.setDeleted(res.getInt("deleted"));
//            }
        }
        catch(SQLException ex){
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void initLatestLoaning(){
        latestLoaning.setId(0);
        latestLoaning.setRoomId(0);
        latestLoaning.setUserId(0);
        latestLoaning.setStartTime("");
        latestLoaning.setEndTime("");
        latestLoaning.setDeleted(0);
    }
    
}
