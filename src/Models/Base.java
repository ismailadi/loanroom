/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Utils.QueryBuilder;
import java.sql.ResultSet;

/**
 *
 * @author Anon
 */
public interface Base {
    public QueryBuilder query=new QueryBuilder();
    public ResultSet getAll();
    public ResultSet get();
    public void insert();
    public void delete(Integer id);
    public void update(Integer id);
}
