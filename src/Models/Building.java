/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class Building implements Base{
    private final String tableName="building";
    private final String vwName="";
    private final LinkedList<Building> data = new LinkedList<>();
    private Building selectedData = this;
    private Integer id;
    private String name;
    private String description;
    private Integer deleted;

    /**
    * No args constructor for use in serialization
    * 
    */
    public Building() {
    }

    /**
    * 
    * @param id
    * @param description
    * @param name
    * @param deleted
    */
    public Building(Integer id, String name, String description, Integer deleted) {
    super();
    this.id = id;
    this.name = name;
    this.description = description;
    this.deleted = deleted;
    }

    public Integer getId() {
    return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

    public String getDescription() {
    return description;
    }

    public void setDescription(String description) {
    this.description = description;
    }

    public Integer getDeleted() {
    return deleted;
    }

    public void setDeleted(Integer deleted) {
    this.deleted = deleted;
    }
    
    public LinkedList getData(){
        return data;
    }
    
    public void addData(Building data){
        this.data.add(data);
    }
    
    public Building getSelectedData(){
        return this.selectedData;
    }
    
    public void setSelectedData(Building param){
        this.selectedData = param;
    }
    
    @Override
    public ResultSet getAll() {
        ResultSet res = null;
        try {
            res = query.getAll(tableName, "where deleted=0 order by id asc");
        } catch (SQLException ex) {
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    @Override
    public ResultSet get(){
        ResultSet res = null;
        try{
            res = query.getAll(tableName, "where deleted=0 AND id='"+id+"' order by id asc");
        }
        catch(SQLException ex){
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    @Override
    public void insert() {
        Object[] obj = {id,name,description,deleted};
        try {
            query.post(tableName,obj);
        } catch (SQLException ex) {
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void delete(Integer id) {
        try {
            query.del(tableName, id);
        } catch (SQLException ex) {
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Integer id) {
        Object[] column = {"name","description","deleted"};
        Object[] data = {name,description,deleted};
        try {
            query.put(tableName, id, column, data);
        } catch (SQLException ex) {
            Logger.getLogger(Building.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public String toString(){
        return this.name;
    }
}
