/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class User implements Base{
    private final String tableName="user";
    private Integer id;
    private String name;
    private String username;
    private String password;
    private Integer level;
    private Integer deleted;

    /**
    * No args constructor for use in serialization
    * 
    */
    public User() {
    }

    /**
    * 
    * @param id
    * @param username
    * @param name
    * @param deleted
    * @param password
    */
    public User(Integer id, String name, String username, String password, Integer level, Integer deleted) {
    super();
    this.id = id;
    this.name = name;
    this.username = username;
    this.password = password;
    this.level = level;
    this.deleted = deleted;
    }

    public Integer getId() {
    return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

    public String getUsername() {
    return username;
    }

    public void setUsername(String username) {
    this.username = username;
    }

    public String getPassword() {
    return password;
    }

    public void setPassword(String password) {
    this.password = password;
    }

    public Integer getLevel(){
        return level;
    }
    
    public void setLevel(Integer level){
        this.level = level;
    }
    public Integer getDeleted() {
    return deleted;
    }

    public void setDeleted(Integer deleted) {
    this.deleted = deleted;
}

    @Override
    public ResultSet getAll() {
        ResultSet res = null;
        try {
            res = query.getAll(tableName, "where deleted=0 order by id asc");
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    @Override
    public ResultSet get() {
        ResultSet res = null;
        try {
            res = query.getAll(tableName, "where deleted=0 AND id='"+id+"' order by id asc");
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    @Override
    public void insert() {
        Object[] obj = {id, name, username, password, level, deleted};
        try {
            query.post(tableName, obj);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            query.del(tableName, id);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Integer id) {
        Object[] column = {"name", "username", "password", "level", "deleted"};
        Object[] data = {name, username, password, level ,deleted};
        try {
            query.put(tableName, id, column, data);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Boolean authLoan(){
        ResultSet res = null;
        try {
            res = query.getAll(tableName, "where deleted=0 AND username='"+username+"' AND password='"+password+"' AND level=1 order by id asc");
            while(res.next()){
                setId(res.getInt("id"));
                setName(res.getString("name"));
                setLevel(res.getInt("level"));
                setDeleted(0);
            }
            return res.first();
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
        
    public Boolean authAdmin(){
        ResultSet res = null;
        try {
            res = query.getAll(tableName, "where deleted=0 AND username='"+username+"' AND password='"+password+"' AND level=0 order by id asc");
            while(res.next()){
                setId(res.getInt("id"));
                setName(res.getString("name"));
                setLevel(res.getInt("level"));
                setDeleted(0);
            }
            return res.first();
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
