/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author isadi
 */
public class Database {
    private Connection connection;
    private static boolean connected=false;
    public Connection connect(){
       //untuk koneksi ke driver
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("berhasil load driver");
        }
        catch(ClassNotFoundException cnfe){
            System.out.println("Tidak ada Driver "+cnfe);
        }

        //untuk koneksi ke database
        try{
            String url="jdbc:mysql://localhost:3306/db_loaning";
            connection=DriverManager.getConnection(url,"root","");
            System.out.println("Koneksi Berhasil");
            if(!connected){
//                JOptionPane.showMessageDialog(null,"Koneksi Berhasil","Peringatan",JOptionPane.INFORMATION_MESSAGE);
            }
            connected = true;
        }
        catch(SQLException se){
            System.out.println("Gagal koneksi "+se);
            JOptionPane.showMessageDialog(null,"Gagal Koneksi Database","Peringatan",JOptionPane.WARNING_MESSAGE);
            System.exit(1);
        }

        return connection;
    }
}
