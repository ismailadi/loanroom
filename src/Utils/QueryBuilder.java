/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import config.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author isadi
 */
public class QueryBuilder {
    private final Connection db;
    private Statement state;
    private PreparedStatement pState;
    
    public QueryBuilder(){
        db = new Database().connect();
        
        try{
            state = db.createStatement(); 
        }
        catch(SQLException e){
            System.out.println("statement query builder -- error -- "+e);
        }
    }
    
    public void post(String table,Object[] data) throws SQLException{
        String param="";
        for(int i=0; i<data.length;i++){
            param+="?";
            if(i != data.length-1){
                param+=",";
            }
        }
        
        String sql="insert into "+table+" values("+param+")";
        System.out.println("sql = "+sql);
        pState = db.prepareStatement(sql);
        for(int i=0; i<data.length; i++){
            if(data[i] instanceof Integer){
                pState.setInt(i+1, (Integer) data[i]);
            }
            else if(data[i] instanceof String){
                pState.setString(i+1, (String) data[i]);
            }
            else if(data[i] instanceof Double){
                pState.setDouble(i+1, (Double) data[i]);
            }
            else if(data[i] instanceof Float){
                pState.setFloat(i+1, (Float) data[i]);
            }
            else if(data[i] instanceof Date){
                pState.setDate(i+1, (java.sql.Date) data[i]);
            }
            else if(data[i] instanceof byte[]){
                pState.setBytes(i+1, (byte[]) data[i]);
            }
            else if(data[i] == null){
                pState.setNull(i+1, Types.INTEGER);
            }
        }
        pState.execute();
        pState.close();
    }
    
    public ResultSet raw(String query) throws SQLException{
        ResultSet res = state.executeQuery(query);
        return res;   
    }
    
    public ResultSet getAll(String table, String option) throws SQLException{
        ResultSet res = state.executeQuery("Select * from "+table+" "+option);
        return res;   
    }
    
    public void del(String table, Integer id) throws SQLException{
        String sql = "";
        switch (table) {
            case "tbl_employee":
                sql = "update "+table+" set deleted = 1 where nip = '"+id+"'";
                break;
            case "tbl_order":
                sql = "update "+table+" set deleted = 1 where start_time = '"+id+"'";
                break;
            default:
                sql = "update "+table+" set deleted = 1 where id = '"+id+"'";
                break;
        }
        state.execute(sql);
    }
    
    public void delManyParams(String table, Object[] col, Object[] data) throws SQLException{
        String sql = "update "+table+" set deleted = 1 where ";
        for(int i=0;i<col.length;i++){
            sql+=col[i]+"='"+data[i]+"'";
            if(i != data.length-1){
                sql+=" AND ";
            }
        }
        System.out.println(sql);
        state.execute(sql);
    }
    
    public void put(String table, Integer id, Object[] col, Object[] data) throws SQLException{
//        String param="";
//        String sql = "";
//        for(int i=0; i<data.length; i++){
//            param+=col[i]+" = '"+data[i]+"'";
//            if(i != data.length-1){
//                param+=",";
//            }
//        }
//        if(table == "tbl_employee"){
//            sql = "update "+table+" set "+param+" where nip = '"+id+"'";
//        }
//        else{
//            sql = "update "+table+" set "+param+" where id = '"+id+"'";
//        }
//        System.out.println(sql);
//        state.execute(sql);
        String sql="update "+table+" set ";
        String param="";
        for(int i=0; i<data.length;i++){
            param+=col[i]+"=?";
            if(i != data.length-1){
                param+=",";
            }
        }
        
        sql+=param+" where id="+id;
        System.out.println("sql = "+sql);
        pState = db.prepareStatement(sql);
        for(int i=0; i<data.length; i++){
            if(data[i] instanceof Integer){
                pState.setInt(i+1, (Integer) data[i]);
            }
            else if(data[i] instanceof String){
                pState.setString(i+1, (String) data[i]);
            }
            else if(data[i] instanceof Double){
                pState.setDouble(i+1, (Double) data[i]);
            }
            else if(data[i] instanceof Float){
                pState.setFloat(i+1, (Float) data[i]);
            }
            else if(data[i] instanceof Date){
                pState.setDate(i+1, (java.sql.Date) data[i]);
            }
            else if(data[i] instanceof byte[]){
                pState.setBytes(i+1, (byte[]) data[i]);
            }
            else if(data[i] == null){
                pState.setNull(i+1, Types.INTEGER);
            }
        }
        pState.execute();
        pState.close();
    }
    
    public PreparedStatement ddlRaw(String sql) throws SQLException{
       return db.prepareStatement(sql);
    }
}
