/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import Models.Building;
import Models.Facility;
import Models.Loaning;
import Models.Room;
import Models.User;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import static java.lang.System.out;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Acer
 */
public class Main extends javax.swing.JFrame implements ActionListener {
    Building buildingObject = new Building();
    Room roomObject = new Room();
    DefaultComboBoxModel comboBoxBuilding = new DefaultComboBoxModel();
    DefaultTableModel defaultTableModelFacilities;
    LinkedList<Room> dataRoomPane = new LinkedList<>();
    Point location;
    MouseEvent pressed;
    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();        
        this.defaultTableModelFacilities = (DefaultTableModel) facilitiesTable.getModel();
        automator();
        initData();
        initComponentsModel();
        jPanel1.setVisible(true);
        endTimeHour.setSelectedIndex(-1);
        endTimeMinute.setSelectedIndex(-1);
    }
    
    private void automator(){
        Timer t = new Timer(2000, new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("automator");
                roomObject.clearData();
                getRoomData();
                initRoomPane();
//                initRoomFacilitei
            }
        });
        
        t.start();
    }
    private void initDataAndComponents(){
        
    }
    
    private void initComponentsModel(){
        initRoomPane();
        jPanel1.setVisible(false);
        jPanel3.setVisible(false);
        jPanel4.setVisible(false);
        jPanel5.setVisible(false);
    }
    
    private void initData(){
        getBuildingData();
        getRoomData();
        System.out.println("initData Ended");
    }
    
    private void getBuildingData(){
        comboBoxBuilding.removeAllElements();
        try{
            ResultSet res = buildingObject.getAll();
            while(res.next()){
                Building dataBuilding = new Building(res.getInt("id"),res.getString("name"),res.getString("description"),res.getInt("deleted"));
                buildingObject.addData(dataBuilding);
                comboBoxBuilding.addElement(dataBuilding);
                building.setModel((DefaultComboBoxModel)comboBoxBuilding);
            }
            if(buildingObject.getData().size() > 0){
                buildingObject.setSelectedData((Building) buildingObject.getData().getFirst());
                System.out.println(buildingObject.getSelectedData().getName());
            }
        }
        catch(SQLException e){
            out.print("getBuildingData() -- Error -- "+e);
        }
    }
    
    private void getRoomData(){
        try{
            ResultSet res = roomObject.getAll();
            while(res.next()){
                Room dataRoom = new Room(res.getInt("id"),res.getString("code"),res.getString("name"),res.getString("description"),res.getInt("coverage"),res.getString("status"),res.getInt("buildingId"),res.getInt("deleted"));
                roomObject.addData(dataRoom);
            }
        }
        catch(SQLException e){
            out.print("getRoomData() -- Error -- "+e);
        }
    }
    
    private void initRoomPane(){
        roomPane.removeAll();
        this.dataRoomPane.clear();
        System.out.println(buildingObject.getSelectedData().getName());
        for(Object dataLoop : roomObject.getData()){
            Room room = (Room) dataLoop;
            room.getLatestLoaningQuery();
            if(Objects.equals(room.getBuildingId(), buildingObject.getSelectedData().getId())){
                dataRoomPane.add(room);
            }
        }
        GridLayout gridLayout = new GridLayout(dataRoomPane.size(), 2, 0, 10);
        roomPane.setLayout(gridLayout);
        for (Object dataLoop : dataRoomPane) {
            Room room = (Room) dataLoop;
            JButton name = new JButton();
            JLabel status = new JLabel();
            String content1 = "<html>"
                + "<body style='background-color: white; width: ";
            String content2 = "'>"
                    + "<h1>Fixed Width</h1>"
                    + "<p>Body width fixed at ";
            String content3
                    = " using CSS.  "
                    + "Java's HTML"
                    + " support includes support"
                    + " for basic CSS.</p>";
            final String content = content1 + 100 + "px" + content2 + 100 + "px" + content3;
            final String buttonContent = "<html><body> <p>"+room.getCode()+"<br>"+room.getName()+"<br>"+room.getStatus()+" </p> </body></html>";
//            name.setText(room.getCode()+" - "+room.getName());
            name.setText(buttonContent);
            name.addActionListener(this);
            name.putClientProperty("data", room);
            
            //            status.setText(obj.getStatus());
            status.setHorizontalAlignment(SwingConstants.CENTER);
//            status.setText(content);
            //            status.setEnabled(false);
            if(room.getStatus().equals("Available")) status.setIcon(new ImageIcon(Class.class.getResource("/Assets/available.png")));
            else if(room.getStatus().equals("Booked")) status.setIcon(new ImageIcon(Class.class.getResource("/Assets/unavailable.png")));
            roomPane.add(name);
            roomPane.add(status);
        }
        roomPane.revalidate();
        roomPane.repaint();
    }
    
    public void initRoomFacilities(){
        try{
            ResultSet res = roomObject.getRoomFacilitiesQuery();
            while(res.next()){
                Facility facility = new Facility(res.getInt("id"), res.getString("name"), res.getString("description"), res.getInt("deleted"), res.getInt("qty"));
                roomObject.addRoomFacilityData(facility);
            }
        }
        catch(SQLException ex){
            out.print("getRoomFacilities() -- Error -- "+ex);
        }
        initRoomFacilitiesLabel();
        initRoomFacilitiesTable();
    }
    
    public void initRoomFacilitiesLabel(){
//        totalFacilitiesLabel.setHorizontalAlignment(SwingConstants.CENTER);
        countFacilitiesLabel.setHorizontalAlignment(SwingConstants.CENTER);
//        Room room = new Room();
//        room.setId(roomObject.getSelectedData().getId());
//        ResultSet res = room.get();
//        try {
//            while(res.next()){
//                Room room2 = new Room(res.getInt("id"),res.getString("code"),res.getString("name"),res.getString("description"),res.getInt("coverage"),res.getString("status"),res.getInt("buildingId"),res.getInt("deleted"));
//                roomObject.setSelectedData(room2);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
//        }
        buildingLabel.setText(this.setToParagraph(buildingObject.getSelectedData().getName(),"font-size:13px; text-align:center;"));
        codeLabel.setText(this.setToParagraph(roomObject.getSelectedData().getCode(),"font-size:13px; text-align:center;"));
        nameLabel.setText(this.setToParagraph(roomObject.getSelectedData().getName(),"font-size:13px; text-align:center;"));
        coverageLabel.setText(this.setToParagraph(roomObject.getSelectedData().getCoverage().toString()+" Orang","font-size:13px; text-align:center;"));
        descriptionLabel.setText(this.setToParagraph(roomObject.getSelectedData().getDescription(),"font-size:10px; text-align:left; width:140px; margin-top:5px;"));
        statusLabel.setText(this.setToParagraph(roomObject.getSelectedData().getStatus(),"font-size:13px; text-align:center;"));
        if(roomObject.getSelectedData().getStatus().equals("Booked")){    
            
            String time = roomObject.getSelectedData().getLatestLoaning().getEndTime().split("\\s")[1].split("\\.")[0].split("\\:")[0]
                            +":"+
                            roomObject.getSelectedData().getLatestLoaning().getEndTime().split("\\s")[1].split("\\.")[0].split("\\:")[1];
            System.out.print(time);
            bookedTime.setText(this.setToParagraph("Until "+time,"font-size:10px; text-align:center;"));
            
        }
        else{
            bookedTime.setText("");
        }
      
        
        jPanel1.setVisible(true);
        jPanel3.setVisible(true);
        jPanel4.setVisible(true);
        if(roomObject.getSelectedData().getStatus().equals("Available")){
            jPanel5.setVisible(true);
        }
        else if(roomObject.getSelectedData().getStatus().equals("Booked")){
            jPanel5.setVisible(false);
        }
    }
    
    public void initRoomFacilitiesTable(){
        defaultTableModelFacilities.setRowCount(0);
        System.out.println(defaultTableModelFacilities.getRowCount());
        Integer count = 0;
        for(Object data : roomObject.getRoomFacilites()){
            Facility obj = (Facility) data;
            defaultTableModelFacilities.addRow(obj.getDataField());
            count += obj.getQty();
        }
        this.countFacilitiesLabel.setText(this.setToParagraph(count.toString(), "font-size:13px; text-align:center;"));
        
//        this.defaultTableModelFacilities.setRowCount(0);
    }
    
    public void initBookingForm(){

    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelParent = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        building = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        buildingLabel = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        descriptionLabel = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        nameLabel = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        coverageLabel = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        codeLabel = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();
        bookedTime = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        countFacilitiesLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        facilitiesTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        roomPane = new javax.swing.JPanel();
        panelForm1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        panelBookingForm1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        username = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        endTimeHour = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        endTimeMinute = new javax.swing.JComboBox<>();
        password = new javax.swing.JPasswordField();
        book = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        panelParent.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)));
        panelParent.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/image/exitic.png"))); // NOI18N
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jLabel1FocusGained(evt);
            }
        });
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel1MouseExited(evt);
            }
        });
        panelParent.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1640, 0, -1, 40));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/image/minimize.png"))); // NOI18N
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel6MouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel6MousePressed(evt);
            }
        });
        panelParent.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1590, 0, -1, 40));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/image/toolbar.png"))); // NOI18N
        jLabel3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jLabel3MouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel3MouseMoved(evt);
            }
        });
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel3MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabel3MouseReleased(evt);
            }
        });
        panelParent.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1690, -1));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMinimumSize(new java.awt.Dimension(1300, 700));
        jPanel1.setPreferredSize(new java.awt.Dimension(1300, 700));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        building.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                buildingItemStateChanged(evt);
            }
        });
        building.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buildingActionPerformed(evt);
            }
        });
        jPanel1.add(building, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 310, -1));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)), "Building", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jPanel6.setLayout(new java.awt.BorderLayout());

        buildingLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        buildingLabel.setText("building");
        jPanel6.add(buildingLabel, java.awt.BorderLayout.CENTER);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)), "Description"));
        jPanel7.setMaximumSize(new java.awt.Dimension(232, 144));
        jPanel7.setLayout(new java.awt.BorderLayout());

        descriptionLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        descriptionLabel.setText("description");
        descriptionLabel.setToolTipText("");
        descriptionLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        descriptionLabel.setAutoscrolls(true);
        descriptionLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel7.add(descriptionLabel, java.awt.BorderLayout.CENTER);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)), "Name"));
        jPanel11.setLayout(new java.awt.BorderLayout());

        nameLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nameLabel.setText("name");
        jPanel11.add(nameLabel, java.awt.BorderLayout.CENTER);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)), "Coverage"));
        jPanel12.setLayout(new java.awt.BorderLayout());

        coverageLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        coverageLabel.setText("coverage");
        jPanel12.add(coverageLabel, java.awt.BorderLayout.CENTER);

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)), "Code"));
        jPanel14.setLayout(new java.awt.BorderLayout());

        codeLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        codeLabel.setText("code");
        jPanel14.add(codeLabel, java.awt.BorderLayout.CENTER);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.lightGray, java.awt.Color.lightGray, java.awt.Color.lightGray, java.awt.Color.lightGray), "Status"));
        jPanel15.setMaximumSize(new java.awt.Dimension(196, 204));
        jPanel15.setLayout(new java.awt.BorderLayout());

        statusLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        statusLabel.setText("status");
        jPanel15.add(statusLabel, java.awt.BorderLayout.CENTER);
        jPanel15.add(bookedTime, java.awt.BorderLayout.PAGE_END);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 870, 230));

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)), "Total Facilities"));
        jPanel10.setLayout(new java.awt.BorderLayout());

        countFacilitiesLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        countFacilitiesLabel.setText("0");
        jPanel10.add(countFacilitiesLabel, java.awt.BorderLayout.CENTER);

        facilitiesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Name", "Description", "Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        facilitiesTable.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        facilitiesTable.setEnabled(false);
        facilitiesTable.setRowHeight(26);
        facilitiesTable.setRowMargin(4);
        jScrollPane1.setViewportView(facilitiesTable);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 846, Short.MAX_VALUE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(173, 173, 173))
        );

        jPanel2.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, 870, 360));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 40, 930, 690));

        roomPane.setBackground(new java.awt.Color(255, 255, 255));
        roomPane.setAlignmentX(0.0F);
        roomPane.setFocusable(false);
        roomPane.setPreferredSize(new java.awt.Dimension(268, 0));
        roomPane.setLayout(new java.awt.GridLayout(1, 3, 5, 20));
        jScrollPane2.setViewportView(roomPane);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 310, 630));

        panelForm1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)), "Loan Form", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        panelBookingForm1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setText("Username");
        panelBookingForm1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, 20));

        username.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usernameActionPerformed(evt);
            }
        });
        panelBookingForm1.add(username, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 30, 200, -1));

        jLabel7.setText("Password");
        panelBookingForm1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, 20));

        jLabel8.setText("End Time");
        panelBookingForm1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, -1, 20));

        endTimeHour.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "23" }));
        panelBookingForm1.add(endTimeHour, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, 40, -1));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText(":");
        panelBookingForm1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 130, 20, 20));

        endTimeMinute.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        panelBookingForm1.add(endTimeMinute, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 130, -1, -1));

        password.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passwordActionPerformed(evt);
            }
        });
        panelBookingForm1.add(password, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 80, 200, -1));

        book.setText("Book");
        book.setEnabled(false);
        book.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bookActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBookingForm1, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(book, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(131, 131, 131))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelBookingForm1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(book, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(160, 160, 160))
        );

        javax.swing.GroupLayout panelForm1Layout = new javax.swing.GroupLayout(panelForm1);
        panelForm1.setLayout(panelForm1Layout);
        panelForm1Layout.setHorizontalGroup(
            panelForm1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelForm1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelForm1Layout.setVerticalGroup(
            panelForm1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelForm1Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(panelForm1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1280, 0, -1, 310));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/image/bg-patern.png"))); // NOI18N
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(1300, 350, -1, -1));

        panelParent.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 1690, 760));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelParent, javax.swing.GroupLayout.PREFERRED_SIZE, 1690, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelParent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buildingItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_buildingItemStateChanged
        if (evt.getStateChange() == evt.SELECTED) {
            Building building = (Building) ((JComboBox) evt.getSource()).getSelectedItem();
            out.println(building.getId()+", "+evt.getStateChange()+" == "+evt.SELECTED);
            buildingObject.setSelectedData(building);
            initRoomPane();
        } else if (evt.getStateChange() == evt.DESELECTED) {
            out.println(evt.getStateChange()+" -- "+evt.DESELECTED);
        }
    }//GEN-LAST:event_buildingItemStateChanged

    private void buildingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buildingActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buildingActionPerformed

    private void bookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bookActionPerformed
        // TODO add your handling code here:());
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date = new Date();
        String startTime = dateFormat2.format(date);
        String endTime = dateFormat.format(date)+" "+endTimeHour.getSelectedItem()+":"+endTimeMinute.getSelectedItem()+":00";
        User user = new User();
        user.setUsername(username.getText());
        user.setPassword(password.getText());
        Loaning loan = new Loaning();
        
        Boolean auth = user.authLoan();
        if(auth){
            loan.setRoomId(this.roomObject.getSelectedData().getId());
            loan.setUserId(user.getId());
            loan.setStartTime(startTime);
            loan.setEndTime(endTime);
            loan.setDeleted(0);
            loan.insert();
            JOptionPane.showMessageDialog(null, "Congratulation :) You have your room now", "Information", HEIGHT);
            roomObject.clearData();
            getRoomData();
            initRoomPane();
            statusLabel.setText(setToParagraph("Booked","font-size:13px; text-align:center;"));
            bookedTime.setText("Until "+endTime);
            
            username.setText("");
            password.setText("");
            endTimeHour.setSelectedIndex(-1);
            endTimeMinute.setSelectedIndex(-1);
            jPanel5.setVisible(false);
        }
        else{
            JOptionPane.showMessageDialog(null, "Uhmm... Your account is not valid", "Caution", HEIGHT);
        }
    }//GEN-LAST:event_bookActionPerformed

    private void usernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_usernameActionPerformed

    private void passwordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passwordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passwordActionPerformed

    private void jLabel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MousePressed
        // TODO add your handling code here:
        pressed = evt;
    }//GEN-LAST:event_jLabel3MousePressed

    private void jLabel3MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseDragged
        // TODO add your handling code here:
//        Component component = evt.getComponent();
//        location = component.getLocation(location);
        int x = evt.getXOnScreen() - pressed.getX();
        int y = evt.getYOnScreen() - pressed.getY();
        this.setLocation(x, y);
    }//GEN-LAST:event_jLabel3MouseDragged

    private void jLabel3MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseMoved
        // TODO add your handling code here:
//        Component component = evt.getComponent();
//        location = component.getLocation(location);
//        int x = location.x - pressed.getX() + evt.getX();
//        int y = location.y - pressed.getY() + evt.getY();
//        this.setLocation(x, y);
    }//GEN-LAST:event_jLabel3MouseMoved

    private void jLabel3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel3MouseReleased

    private void jLabel1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jLabel1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel1FocusGained

    private void jLabel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseEntered
        // TODO add your handling code here:
        jLabel1.setIcon(new ImageIcon(Class.class.getResource("/Assets/image/exitic2.png")));
    }//GEN-LAST:event_jLabel1MouseEntered

    private void jLabel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseExited
        // TODO add your handling code here:
        jLabel1.setIcon(new ImageIcon(Class.class.getResource("/Assets/image/exitic.png")));
    }//GEN-LAST:event_jLabel1MouseExited

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        // TODO add your handling code here:
        jLabel1.setIcon(new ImageIcon(Class.class.getResource("/Assets/image/exitic.png")));
//        System.exit(0);
        dispose();
    }//GEN-LAST:event_jLabel1MouseClicked

    private void jLabel6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseEntered
        // TODO add your handling code here:
        jLabel6.setIcon(new ImageIcon(Class.class.getResource("/Assets/image/minimize2.png")));
    }//GEN-LAST:event_jLabel6MouseEntered

    private void jLabel6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseExited
        // TODO add your handling code here:
        jLabel6.setIcon(new ImageIcon(Class.class.getResource("/Assets/image/minimize.png")));
    }//GEN-LAST:event_jLabel6MouseExited

    private void jLabel6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MousePressed
        // TODO add your handling code here:
        jLabel6.setIcon(new ImageIcon(Class.class.getResource("/Assets/image/minimize.png")));
        setState(ICONIFIED);
    }//GEN-LAST:event_jLabel6MousePressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton book;
    private javax.swing.JLabel bookedTime;
    private javax.swing.JComboBox<String> building;
    private javax.swing.JLabel buildingLabel;
    private javax.swing.JLabel codeLabel;
    private javax.swing.JLabel countFacilitiesLabel;
    private javax.swing.JLabel coverageLabel;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JComboBox<String> endTimeHour;
    private javax.swing.JComboBox<String> endTimeMinute;
    private javax.swing.JTable facilitiesTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JPanel panelBookingForm1;
    private javax.swing.JPanel panelForm1;
    private javax.swing.JPanel panelParent;
    private javax.swing.JPasswordField password;
    private javax.swing.JPanel roomPane;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JTextField username;
    // End of variables declaration//GEN-END:variables

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        JButton source = (JButton) e.getSource();
        roomObject.clearRoomFacilities();
        roomObject.setSelectedData((Room) source.getClientProperty("data"));
        if(roomObject.getSelectedData().getStatus().equals("Booked")) book.setEnabled(false);
        else if(roomObject.getSelectedData().getStatus().equals("Available")) book.setEnabled(true);
        initRoomFacilities();
    }
    
    private Object[] appendValue(Object[] obj, Object newObj) {
	ArrayList<Object> temp = new ArrayList<>(Arrays.asList(obj));
	temp.add(newObj);
	return temp.toArray();
    }
    
    public String setToParagraph(String text, String style){
        String paragraph = "<html> <p style='"+style+"'>"+text+"</p></html>";
        return paragraph;
    }
}
