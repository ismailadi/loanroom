/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import Models.Building;
import Models.Facility;
import Models.Room;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import static java.lang.System.out;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Acer
 */
public class ViewRoom extends javax.swing.JFrame implements ActionListener{
    Building buildingObject = new Building();
    Room roomObject = new Room();
    DefaultComboBoxModel comboBoxBuilding = new DefaultComboBoxModel();
    DefaultTableModel defaultTableModelFacilities;
    /**
     * Creates new form ViewRoom
     */
    public ViewRoom() {
        initComponents();
        
        this.defaultTableModelFacilities = (DefaultTableModel) facilitiesTable.getModel();
        initData();
        initComponentsModel();
    }
    
    private void initComponentsModel(){
        initRoomPane();
        jPanel3.setVisible(false);
        jPanel4.setVisible(false);
    }
    
    private void initData(){
        getBuildingData();
        getRoomData();
        System.out.println("initData Ended");
    }
    
    private void getBuildingData(){
        comboBoxBuilding.removeAllElements();
        try{
            ResultSet res = buildingObject.getAll();
            while(res.next()){
                Building dataBuilding = new Building(res.getInt("id"),res.getString("name"),res.getString("description"),res.getInt("deleted"));
                buildingObject.addData(dataBuilding);
                comboBoxBuilding.addElement(dataBuilding);
                building.setModel((DefaultComboBoxModel)comboBoxBuilding);
            }
            if(buildingObject.getData().size() > 0){
                buildingObject.setSelectedData((Building) buildingObject.getData().getFirst());
                System.out.println(buildingObject.getSelectedData().getName());
            }
        }
        catch(SQLException e){
            out.print("getBuildingData() -- Error -- "+e);
        }
    }
    
    private void getRoomData(){
        try{
            ResultSet res = roomObject.getAll();
            while(res.next()){
                Room dataRoom = new Room(res.getInt("id"),res.getString("code"),res.getString("name"),res.getString("description"),res.getInt("coverage"),res.getString("status"),res.getInt("buildingId"),res.getInt("deleted"));
                roomObject.addData(dataRoom);
            }
        }
        catch(SQLException e){
            out.print("getRoomData() -- Error -- "+e);
        }
    }
    
    private void initRoomPane(){
        roomPane.removeAll();
        LinkedList<Room> dataRoomPane = new LinkedList<>();
        System.out.println(buildingObject.getSelectedData().getName());
        for(Object dataLoop : roomObject.getData()){
            Room room = (Room) dataLoop;
            if(Objects.equals(room.getBuildingId(), buildingObject.getSelectedData().getId())){
                dataRoomPane.add(room);
            }
        }
        GridLayout gridLayout = new GridLayout(dataRoomPane.size(), 2, 0, 10);
        roomPane.setLayout(gridLayout);
        for (Object dataLoop : dataRoomPane) {
            Room room = (Room) dataLoop;
            JButton name = new JButton();
            JLabel status = new JLabel();
            String content1 = "<html>"
                + "<body style='background-color: white; width: ";
            String content2 = "'>"
                    + "<h1>Fixed Width</h1>"
                    + "<p>Body width fixed at ";
            String content3
                    = " using CSS.  "
                    + "Java's HTML"
                    + " support includes support"
                    + " for basic CSS.</p>";
            final String content = content1 + 100 + "px" + content2 + 100 + "px" + content3;
            final String buttonContent = "<html><body> <p>"+room.getCode()+"<br>"+room.getName()+"<br>"+room.getStatus()+" </p> </body></html>";
//            name.setText(room.getCode()+" - "+room.getName());
            name.setText(buttonContent);
            name.addActionListener(this);
            name.putClientProperty("data", room);
            
            //            status.setText(obj.getStatus());
            status.setHorizontalAlignment(SwingConstants.CENTER);
//            status.setText(content);
            //            status.setEnabled(false);
            if(room.getStatus().equals("Available")) status.setIcon(new ImageIcon(Class.class.getResource("/Assets/available.png")));
            else if(room.getStatus().equals("Booked")) status.setIcon(new ImageIcon(Class.class.getResource("/Assets/unavailable.png")));
            roomPane.add(name);
            roomPane.add(status);
        }
        roomPane.revalidate();
        roomPane.repaint();
    }
    
    public void initRoomFacilities(){
        try{
            ResultSet res = roomObject.getRoomFacilitiesQuery();
            while(res.next()){
                Facility facility = new Facility(res.getInt("id"), res.getString("name"), res.getString("description"), res.getInt("deleted"), res.getInt("qty"));
                roomObject.addRoomFacilityData(facility);
            }
        }
        catch(SQLException ex){
            out.print("getRoomFacilities() -- Error -- "+ex);
        }
        initRoomFacilitiesLabel();
        initRoomFacilitiesTable();
    }
    
    public void initRoomFacilitiesLabel(){
        totalFacilitiesLabel.setHorizontalAlignment(SwingConstants.CENTER);
        countFacilitiesLabel.setHorizontalAlignment(SwingConstants.CENTER);
        buildingLabel.setText(buildingObject.getSelectedData().getName());
        codeLabel.setText(roomObject.getSelectedData().getCode());
        nameLabel.setText(roomObject.getSelectedData().getName());
        coverageLabel.setText(roomObject.getSelectedData().getCoverage().toString()+" Orang");
        descriptionLabel.setText(roomObject.getSelectedData().getDescription());
        statusLabel.setText(roomObject.getSelectedData().getStatus());
        jPanel3.setVisible(true);
        jPanel4.setVisible(true);
    }
    
    public void initRoomFacilitiesTable(){
        defaultTableModelFacilities.setRowCount(0);
        System.out.println(defaultTableModelFacilities.getRowCount());
        for(Object data : roomObject.getRoomFacilites()){
            Facility obj = (Facility) data;
            defaultTableModelFacilities.addRow(obj.getDataField());
        }
        
//        this.defaultTableModelFacilities.setRowCount(0);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        building = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        buildingLabel = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        descriptionLabel = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        nameLabel = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        coverageLabel = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        codeLabel = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        totalFacilitiesLabel = new javax.swing.JLabel();
        countFacilitiesLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        facilitiesTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        roomPane = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMinimumSize(new java.awt.Dimension(1300, 700));
        jPanel1.setPreferredSize(new java.awt.Dimension(1300, 700));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        building.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                buildingItemStateChanged(evt);
            }
        });
        building.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buildingActionPerformed(evt);
            }
        });
        jPanel1.add(building, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 310, -1));
        building.getAccessibleContext().setAccessibleName("");
        building.getAccessibleContext().setAccessibleDescription("");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        buildingLabel.setText("building");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(buildingLabel)
                .addContainerGap(81, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(buildingLabel)
                .addContainerGap(42, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        descriptionLabel.setText("description");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addComponent(descriptionLabel)
                .addContainerGap(80, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addComponent(descriptionLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        nameLabel.setText("name");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addComponent(nameLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(nameLabel)
                .addContainerGap(41, Short.MAX_VALUE))
        );

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));

        coverageLabel.setText("coverage");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addComponent(coverageLabel)
                .addContainerGap(83, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(coverageLabel)
                .addGap(38, 38, 38))
        );

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));

        codeLabel.setText("code");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addComponent(codeLabel)
                .addContainerGap(102, Short.MAX_VALUE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(codeLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));

        statusLabel.setText("status");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(76, 76, 76)
                .addComponent(statusLabel)
                .addContainerGap(86, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(statusLabel)
                .addGap(93, 93, 93))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 870, 230));

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 420, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 130, Short.MAX_VALUE)
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setLayout(new java.awt.GridLayout());

        totalFacilitiesLabel.setText("Total Facilities");
        totalFacilitiesLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel9.add(totalFacilitiesLabel);

        countFacilitiesLabel.setText("0");
        jPanel9.add(countFacilitiesLabel);

        facilitiesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Name", "Description", "Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(facilitiesTable);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(171, 171, 171))
        );

        jPanel2.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, 870, 360));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 40, 930, 690));

        roomPane.setBackground(new java.awt.Color(255, 255, 255));
        roomPane.setAlignmentX(0.0F);
        roomPane.setFocusable(false);
        roomPane.setPreferredSize(new java.awt.Dimension(268, 0));
        roomPane.setLayout(new java.awt.GridLayout(1, 3, 5, 20));
        jScrollPane2.setViewportView(roomPane);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 310, 630));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 755, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buildingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buildingActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buildingActionPerformed

    private void buildingItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_buildingItemStateChanged
        if (evt.getStateChange() == evt.SELECTED) {
            Building building = (Building) ((JComboBox) evt.getSource()).getSelectedItem();
            out.println(building.getId()+", "+evt.getStateChange()+" == "+evt.SELECTED);
            buildingObject.setSelectedData(building);
            initRoomPane();
        } else if (evt.getStateChange() == evt.DESELECTED) {
            out.println(evt.getStateChange()+" -- "+evt.DESELECTED);
        }
    }//GEN-LAST:event_buildingItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewRoom.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewRoom.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewRoom.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewRoom.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewRoom().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> building;
    private javax.swing.JLabel buildingLabel;
    private javax.swing.JLabel buildingLabel1;
    private javax.swing.JLabel codeLabel;
    private javax.swing.JLabel codeLabel1;
    private javax.swing.JLabel countFacilitiesLabel;
    private javax.swing.JLabel coverageLabel;
    private javax.swing.JLabel coverageLabel1;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JLabel descriptionLabel1;
    private javax.swing.JTable facilitiesTable;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JLabel nameLabel1;
    private javax.swing.JPanel roomPane;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JLabel totalFacilitiesLabel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        JButton source = (JButton) e.getSource();
        roomObject.clearRoomFacilities();
        roomObject.setSelectedData((Room) source.getClientProperty("data"));
        System.out.println(roomObject.getSelectedData().getCode());
        initRoomFacilities();
    }
    
    private Object[] appendValue(Object[] obj, Object newObj) {
	ArrayList<Object> temp = new ArrayList<>(Arrays.asList(obj));
	temp.add(newObj);
	return temp.toArray();
    }
}
